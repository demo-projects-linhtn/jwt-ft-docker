FROM adoptopenjdk/openjdk11:jre-11.0.6_10-alpine
MAINTAINER link2mountain
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} jwt-ft-docker.jar
ENTRYPOINT ["java","-jar","/jwt-ft-docker.jar"]