package com.demo.jwtftdocker.constant;

/**
 * Created by linhtn on 7/12/2021.
 */
public enum RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}
