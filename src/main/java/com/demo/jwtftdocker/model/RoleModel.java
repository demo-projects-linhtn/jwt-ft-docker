package com.demo.jwtftdocker.model;

import com.demo.jwtftdocker.constant.RoleName;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by linhtn on 7/12/2021.
 */
@Entity
@Table(name = "roles")
@Data
public class RoleModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private RoleName name;

}
