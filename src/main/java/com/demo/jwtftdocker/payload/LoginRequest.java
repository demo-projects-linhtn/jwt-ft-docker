package com.demo.jwtftdocker.payload;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by linhtn on 7/16/2021.
 */
@Getter
@Setter
public class LoginRequest {
    private String username;
    private String password;
}
