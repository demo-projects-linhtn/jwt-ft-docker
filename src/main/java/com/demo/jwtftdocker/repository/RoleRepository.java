package com.demo.jwtftdocker.repository;

import com.demo.jwtftdocker.constant.RoleName;
import com.demo.jwtftdocker.model.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by linhtn on 7/12/2021.
 */
@Repository
public interface RoleRepository extends JpaRepository<RoleModel, Long> {
    Optional<RoleModel> findByName(RoleName roleName);
}
