create database tieng_anh_xam_xi_db;
create user tienganhxamxi with password '123';

create table IF NOT EXISTS users(
                                    id numeric primary key not null,
                                    name varchar(50),
    username varchar(50) not null,
    email varchar(50) not null,
    password text not null,
    roles varchar not null
    );

create table IF NOT EXISTS roles(
                                    id numeric primary key not null,
                                    name varchar(50)
    );